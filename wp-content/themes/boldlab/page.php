<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default. Please note that
 * this is the WordPress construct of pages: specifically, posts with a post
 * type of `page`.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BoldLab
 */

get_header();
?>
<section id="primary">
	<main id="main" class="post">
		<?php
			if( tr_show_page_builder("use_builder") ) { 
				tr_components_field('builder');
			} else { 
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/content/content', 'page' );

					// If comments are open, or we have at least one comment, load
					// the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}

				endwhile; // End of the loop.
			}
		?>
	</main><!-- #main -->
</section><!-- #primary -->

<?php
get_footer();
