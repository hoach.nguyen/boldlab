<?php

/**
 * Template Name: Archive Template
 *
 * This is the template that displays all pages by default. Please note that
 * this is the WordPress construct of pages: specifically, posts with a post
 * type of `page`.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BoldLab
 */

get_header(); ?>

<?php
$years = $wpdb->get_results("
    SELECT YEAR(post_date) AS year, COUNT(ID) AS post_count
    FROM $wpdb->posts
    WHERE post_type = 'post' AND post_status = 'publish'
    GROUP BY YEAR(post_date)
    ORDER BY post_date DESC
");
?>
<?php if(is_year()) {
    $current_year = get_query_var('year');
    echo '<input type="hidden" name="year" value="'.$current_year.'">';
    $args = array(
         'post_type' => 'post',
         'posts_per_page' => get_option('posts_per_page'),
         'year' => $current_year,
     );
    $query = new WP_Query($args);    
    if ($query->have_posts()) :
        echo '<div class="flex flex-col gap-10 mt-10" data-scroll-post="true" id="wrapper-post">';
            while( $query->have_posts() ) { $query->the_post();
                echo '<div class="post">
                    <h2><a href="'.get_permalink(get_the_ID()).'" title="'.get_the_title(get_the_ID()).'">'.get_the_title().'</a></h2>';
                    the_content(
                        sprintf(
                            wp_kses(
                                /* translators: %s: Name of current post. Only visible to screen readers. */
                                __( 'Continue reading<span class="sr-only"> "%s"</span>', 'boldlab' ),
                                array(
                                    'span' => array(
                                        'class' => array(),
                                    ),
                                )
                            ),
                            get_the_title()
                        )
                    );
                    echo '<div class="my-[1.1em] date-post uppercase leading-7 font-SourceSansPro border-t border-t-black w-full text-[0.8em] font-semibold text-black">
                        '.get_the_date('F j, Y').'
                    </div>';
                    if(function_exists('share_social')){share_social(get_the_ID());}
                echo '</div>';
            }
        echo '</div>';			
        wp_reset_postdata();
    else :
        echo '<p>' . __('No posts found') . '</p>';
    endif;

} else if(is_month()) {
    $current_month = get_query_var('monthnum');
    $current_year = get_query_var('year');
    echo '<input type="hidden" name="year" value="'.$current_year.'">';
    echo '<input type="hidden" name="month" value="'.$current_month.'">';
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => get_option('posts_per_page'),
        'year' => $current_year,
        'monthnum' => $current_month,
    );
   $query = new WP_Query($args);    
   if ($query->have_posts()) :
       echo '<div class="flex flex-col gap-10 mt-10" data-scroll-post="true" id="wrapper-post">';
           while( $query->have_posts() ) { $query->the_post();
               echo '<div class="post">
                   <h2><a href="'.get_permalink(get_the_ID()).'" title="'.get_the_title(get_the_ID()).'">'.get_the_title().'</a></h2>';
                   the_content(
                       sprintf(
                           wp_kses(
                               /* translators: %s: Name of current post. Only visible to screen readers. */
                               __( 'Continue reading<span class="sr-only"> "%s"</span>', 'boldlab' ),
                               array(
                                   'span' => array(
                                       'class' => array(),
                                   ),
                               )
                           ),
                           get_the_title()
                       )
                   );
                   echo '<div class="my-[1.1em] date-post uppercase leading-7 font-SourceSansPro border-t border-t-black w-full text-[0.8em] font-semibold text-black">
                       '.get_the_date('F j, Y').'
                   </div>';
                   if(function_exists('share_social')){share_social(get_the_ID());}
               echo '</div>';
           }
       echo '</div>';			
       wp_reset_postdata();
   else :
       echo '<p>' . __('No posts found') . '</p>';
   endif;
} else if(is_category()) { 
    $categories = get_the_category();
	$queried_object = get_queried_object();
    $cat_name = $queried_object->name;
	$taxonomy = $queried_object->taxonomy;
	$term_id = $queried_object->term_id;
	$post_types = get_post_type();

	echo '<input type="hidden" name="post_types" value="'.$post_types.'">';
	echo '<input type="hidden" name="taxonomy" value="'.$taxonomy.'">';
	echo '<input type="hidden" name="term_id" value="'.$term_id.'">';
	
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	$posts_per_page = get_option('posts_per_page');
    ?>
    <div class="post">
    <h2 class="capitalize font-bold"><?php echo $cat_name; ?></h2>
    <p class="!mt-1"><?php $des_cat = tr_term_field('description', 'category', $term_id);echo $des_cat; ?></p>
    <?php 
    
	$query_args = array(
		'post_type' => 'post',
		'post_status' => 'publish',
		'orderby' => 'date',
		'order' => 'DESC',
		'paged'  => $paged, 
		'posts_per_page'=> $posts_per_page,
	);
	if (!empty($taxonomy) && !empty($term_id)) {
        $query_args['tax_query'] = array(
            array(
                'taxonomy' => $taxonomy,
                'field'    => 'id',
                'terms'    => $term_id,
            ),
        );
    }
	$query_post = new WP_Query( $query_args);
    
	if( $query_post->have_posts() ) {
		echo '<div class="flex flex-col gap-[30px]" data-scroll-post="true" id="wrapper-post-cat">';
			while( $query_post->have_posts() ) { $query_post->the_post();
				echo '<div class="post">
					<div>
						<h2><a href="'.get_permalink(get_the_ID()).'" title="'.get_the_title(get_the_ID()).'">'.get_the_title().'</a></h2>
					</div>';
					the_content(
						sprintf(
							wp_kses(
								/* translators: %s: Name of current post. Only visible to screen readers. */
								__( 'Continue reading<span class="sr-only"> "%s"</span>', 'boldlab' ),
								array(
									'span' => array(
										'class' => array(),
									),
								)
							),
							get_the_title()
						)
					);
					echo '<div class="my-[1.1em] date-post uppercase leading-7 font-SourceSansPro border-t border-t-black w-full text-[0.8em] font-semibold text-black">
						'.get_the_date('F j, Y').'
					</div>';
					if(function_exists('share_social')){share_social(get_the_ID());}
				echo '</div>';
			}
		echo '</div>';
		
		wp_reset_postdata();
	}
    echo '</div>';
} else {?>
    <section id="primary" class="post archive">
        <h2 class="capitalize"><?php echo get_the_title(); ?></h2>
        <p class="!mt-1"><?php echo esc_html__('Explore the archives by year and month.', 'BoldLab'); ?></p>
        <?php
        echo '<ul class="collaps-arch !list-none !pl-0">';
        $stt = 1;
        foreach ($years as $year) {
            echo '<li class="collapsing-archives ' . ($stt === 1 ? "collapsed" : "expand") . ' ">
                <span class="collapsing-archives cursor-pointer ' . ($stt === 1 ? "collapsed" : "expand") . '" title="click to ' . ($stt === 1 ? "collapse" : "expand") . '">' . ($stt === 1 ? "▼" : "►") . '</span>
                <a href="' . get_year_link($year->year) . '" title="' . $year->year . '">' . $year->year . '<span class="pl-1 yearcount">(' . $year->post_count . ')</span></a>
                <div class="' . ($stt === 1 ? "" : "hidden") . '">';
            $months = $wpdb->get_results("
                SELECT MONTH(post_date) AS month, COUNT(ID) AS post_count
                FROM $wpdb->posts
                WHERE post_type = 'post' AND post_status = 'publish'
                AND YEAR(post_date) = '" . $year->year . "'
                GROUP BY MONTH(post_date)
                ORDER BY post_date DESC
            ");
            echo '<ul class="!list-none ml-4 !pl-0 mt-3">';
            foreach ($months as $month) {
                echo '<li class="collapsing-archives !text-size-inherit expand">
                <span class="collapsing-archives cursor-pointer expand" title="click to expand">►</span>
                <a href="' . get_month_link($year->year, $month->month) . '" title="' . date('F', mktime(0, 0, 0, $month->month, 1, $year->year)) . '">' . date('F', mktime(0, 0, 0, $month->month, 1, $year->year)) . ' (<span class="monthcount">' . $month->post_count . '</span>)</a>
                <div class="hidden">';
                $args = array(
                    'post_type' => 'post',
                    'posts_per_page' => -1,
                    'year' => $year->year,
                    'monthnum' => $month->month,
                );
                $query = new WP_Query($args);
                if ($query->have_posts()) {
                    echo '<ul class="!list-none ml-4 pl-4 mt-3">';
                    while ($query->have_posts()) {
                        $query->the_post();
                        echo '<li class="collapsing-archives !text-size-inherit item">
                            <a href="' . get_permalink() . '" title="' . get_the_title() . '">' . get_the_title() . '</a>
                        </li>';
                    }
                    echo '</ul>';
                }
                echo '</div></li>';
            }
            echo '</ul>';
            echo '</div></li>';
            $stt++;
        }
        echo '</ul>';
        ?>
    </section>
<?php }?>
<?php
get_footer();
