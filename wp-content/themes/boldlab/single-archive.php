<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package BoldLab
 */

get_header();
?>
	<div class="post">
		<?php echo '<h2>'.get_the_title().'</h2>'; ?>
		<?php 
			the_content(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers. */
						__( 'Continue reading<span class="sr-only"> "%s"</span>', 'boldlab' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				)
			);
		
		echo '<div class="my-[1.1em] date-post uppercase leading-7 font-SourceSansPro border-t border-t-black w-full text-[0.8em] font-semibold text-black">
			'.get_the_date('F j, Y').'
		</div>';
		if(function_exists('share_social')){share_social(get_the_ID());}
		?>
	</div>
	<?php
		$terms = get_the_terms(get_the_ID(), 'archive_categories');
		$term_ids = $terms ? wp_list_pluck($terms, 'term_id') : array();
		
		$prev_post = get_previous_post();
		if ($prev_post) {
			$prev_post_id = $prev_post->ID;
		} else {
			$prev_post_id = null;
		}
		
		// Lấy bài viết kế tiếp
		$next_post = get_next_post();
		if ($next_post) {
			$next_post_id = $next_post->ID;
		} else {
			$next_post_id = null;
		}
		
		// Lấy bài viết ngẫu nhiên
		$random_post_args = array(
			'post_type'      => 'archive', // Thay 'your_post_type' bằng post type bạn muốn sử dụng
			'posts_per_page' => 1,
			'orderby'        => 'rand',
			'tax_query'      => array(
				array(
					'taxonomy' => 'archive_categories', // Thay 'your_taxonomy' bằng taxonomy bạn muốn sử dụng
					'field'    => 'term_id',
					'terms'    => $term_ids,
				),
			),
		);
		// $random_post = get_posts(array('orderby' => 'rand', 'posts_per_page' => 1));
		$random_posts = get_posts($random_post_args);
		$random_post_id = $random_posts ? $random_posts[0]->ID : null;
	?>
	<!-- Liên kết đến bài viết trước đó -->
	<div class="grid grid-cols-3 gap-5 mt-8 mb-12">
		<?php if ($prev_post_id) : ?>
			<a class="bg-black block text-center text-white no-underline hover:bg-[#1e2b38] transition-all hover:no-underline border border-[#2c3e50] shadow-lg rounded-lg py-2.5 px-5 tracking-[0.02em] uppercase font-bold text-sm" href="<?php echo get_permalink($prev_post_id); ?>">Back</a>
		<?php endif; ?>
		<!-- Liên kết đến bài viết ngẫu nhiên -->
		<?php if ($random_post_id) : ?>
			<a class="bg-black block text-center text-white no-underline hover:bg-[#1e2b38] transition-all hover:no-underline border border-[#2c3e50] shadow-lg rounded-lg py-2.5 px-5 tracking-[0.02em] uppercase font-bold text-sm" href="<?php echo get_permalink($random_post_id); ?>">Random</a>
		<?php endif; ?>
		<!-- Liên kết đến bài viết kế tiếp -->
		<?php if ($next_post_id) : ?>
			<a class="bg-black block text-center text-white no-underline hover:bg-[#1e2b38] transition-all hover:no-underline border border-[#2c3e50] shadow-lg rounded-lg py-2.5 px-5 tracking-[0.02em] uppercase font-bold text-sm" href="<?php echo get_permalink($next_post_id); ?>">Next</a>
		<?php endif; ?>

	</div>


<?php
get_footer();
