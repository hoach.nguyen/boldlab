<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BoldLab
 */

get_header();
$post_types = get_post_type();
?>
	<section id="primary" class="post archive">
		<h2 class="capitalize"><?php echo $post_types;?></h2>
		<p class="!mt-1"><?php echo esc_html__( 'Explore the archives by year and month.', 'BoldLab' ); ?></p>
		<?php
			// Chọn taxonomy bạn quan tâm
			$taxonomy = 'archive_categories';

			// Lấy tất cả các term cha (có parent là 0)
			$parent_terms = get_terms([
				'taxonomy' => $taxonomy,
				'parent' => 0,
			]);

			// Lặp qua từng term cha
			if($parent_terms) {
				$stt = 1;
				echo '<ul class="collaps-arch !list-none !pl-0">';
					foreach ($parent_terms as $parent_term) {
						echo '<li class="collapsing-archives '.($stt === 1 ? "collapsed":"expand").' ">
							<span class="collapsing-archives cursor-pointer '.($stt === 1 ? "collapsed":"expand").'" title="click to '.($stt === 1 ? "collapse":"expand").'">'.($stt === 1 ? "▼":"►").'</span>
							<a href="'.esc_url(get_term_link($parent_term)).'" title="'.$parent_term->name.'">'.$parent_term->name.'<span class="pl-1 yearcount">('.$parent_term->count.')</span></a>
							<div class="'.($stt === 1 ? "":"hidden").'">';
						// Lấy tất cả các term con của term cha hiện tại
						$child_terms = get_terms([
							'taxonomy' => $taxonomy,
							'parent' => $parent_term->term_id,
						]);
						if($child_terms) {
							echo '<ul class="!list-none ml-4 !pl-0 mt-3">';
								// Lặp qua từng term con
								foreach ($child_terms as $child_term) {
									echo '<li class="collapsing-archives !text-size-inherit expand">
									<span class="collapsing-archives cursor-pointer expand" title="click to expand">►</span>
									<a href="'.esc_url(get_term_link($child_term)).'" title="'.$child_term->name.'">'.$child_term->name.'<span class="monthcount">('.$child_term->count.')</span></a>
									<div class="hidden">';
										$args = array(
											'post_type'      => 'archive', // Điều này có thể thay đổi tùy vào loại bài viết của bạn
											'post_status' => 'publish',
											'posts_per_page' => -1,
											'tax_query'      => array(
												array(
													'taxonomy' => $taxonomy,
													'field'    => 'term_id',
													'terms'    => $child_term->term_id,
												),
											),
										);
										$query_post = new WP_Query($args);
										if( $query_post->have_posts() ) {
											echo '<ul class="!list-none ml-4 pl-4 mt-3">';
												while( $query_post->have_posts() ) { $query_post->the_post();
													echo '<li class="collapsing-archives !text-size-inherit item">
														<a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a>
													</li>';
												}
											echo '</ul>';
										}
									echo '</div></li>';
								}
							echo '</ul>';
						}
						echo '</div></li>';
						$stt ++;
					}
				echo '</ul>';
			}

		?>
	</section>
<?php
get_footer();
