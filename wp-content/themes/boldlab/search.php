<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package BoldLab
 */

get_header();$unique_id_s = esc_attr( uniqid( 'search-form-' ) );
?>

	<section id="primary">
		<main id="main">

		<?php if ( have_posts() ) : ?>

			<?php
			printf(
				/* translators: 1: search result title. 2: search term. */
				'<h2 class="mb-4 font-SourceSansPro max-sm:text-[1.8em] text-[2.2em] font-black leading-[1.1em] -tracking-[0.02em] text-black">%1$s</h2>',
				esc_html__( 'Search', 'boldlab' ),
				get_search_query()
			);
			?>
			<form role="search" method="get" class="search-form font-SourceSansPro mb-8" action="<?php echo esc_url( home_url( '/' ) ); ?>">
				<div class="relative border border-black rounded-[5px] flex overflow-hidden">
					<input id="<?php echo $unique_id_s; ?>" class="search-field border-none bg-transparent h-9 w-full max-w-[calc(100%-62px)] p-[5px] outline-none text-sm" type="search" value="<?php echo get_search_query(); ?>" name="s" required />
					<button type="submit" class="search-submit text-white bg-black border-0 h-9 min-w-[62px] flex justify-center items-center flex-shrink-0">
						<svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" viewBox="-2972 1546 21.43 21.43"><g transform="translate(-3222 879)"><path style="fill:#fff;" d="M21.43,19.755l-6.362-6.529a8.1,8.1,0,0,0,1.674-4.855A8.4,8.4,0,0,0,8.371,0,8.29,8.29,0,0,0,0,8.371a8.4,8.4,0,0,0,8.371,8.371,8.1,8.1,0,0,0,4.855-1.674l6.362,6.362ZM1.674,8.371a6.7,6.7,0,1,1,6.7,6.7A6.716,6.716,0,0,1,1.674,8.371Z" transform="translate(250 667)"/></g></svg>	
					</button>
				</div>
			</form>
			<div class="search-results flex flex-col gap-4 text-[15px] font-SourceSansPro">
				<?php // Start the Loop.
					while ( have_posts() ) :
						the_post();
						//get_template_part( 'template-parts/content/content', 'excerpt' );
						echo '<div><a class="text-black font-bold" href="'.esc_url( get_permalink() ).'" title="'.get_the_title().'">'.get_the_title().'</a>';
						echo '<div>';
							the_excerpt();
						echo '</div></div>';

						// End the loop.
					endwhile;

					// Previous/next page navigation.
					//boldlab_the_posts_navigation();
					echo '<div class="my-[1.1em] leading-7 border-t border-t-black w-full text-sm text-black">
						You are at the end of the results.
					</div>';
				?>
			</div>
			<?php
		else :

			// If no content is found, get the `content-none` template part.
			get_template_part( 'template-parts/content/content', 'none' );

		endif;
		?>
		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
