<?php
/**
 * Template Name: Confirm Template
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no `home.php` file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BoldLab
 */

get_header();
$order_code = isset($_GET['order_code']) ? $_GET['order_code'] : '';
$r_quote = isset($_GET['r_quote']) ? $_GET['r_quote'] : '';
$term_id='';
$name_cat;
if($r_quote) {
    $term_slug = $r_quote;
    $taxonomy = 'products_vietan';    
    $term = get_term_by('slug', $term_slug, $taxonomy);
    
    if ($term) { $term_id = $term->term_id;$name_cat = $term->name; }
}
$product_banner_cover_id = tr_term_field('products_vietan_cover_banner', 'category', $term_id); 
$product_banner_cover_sizes = wp_get_attachment_image_sizes( $product_banner_cover_id, 'full' ); // Lấy tất cả các kích thước được cấu hình trong phương tiện truyền thông
$product_banner_cover_srcset = wp_get_attachment_image_srcset( $product_banner_cover_id, $product_banner_cover_sizes ); // Lấy srcset attribute
$product_banner_cover_src = wp_get_attachment_image_url( $product_banner_cover_id, 'thumbnail' ); // Lấy đường dẫn đầy đủ của hình ảnh
$product_banner_cover_alt = get_post_meta( $product_banner_cover_id, '_wp_attachment_image_alt', true );
$image_attributes = wp_get_attachment_image_src($product_banner_cover_id, 'large');
$width='auto';$height='auto';
if ($image_attributes) {
    $width = $image_attributes[1];
    $height = $image_attributes[2];                            
}
?>
<section class="relative flex items-center py-5 sm:py-10 lg:py-20 min-h-[156px] md:min-h-[282px]">
    <img <?php echo 'width="'.$width.'" height="'.$height.'"'; ?> class="w-full h-full object-cover absolute top-1/2 left-0 right-0 -translate-y-1/2" src="<?php echo $product_banner_cover_id ? $product_banner_cover_src : get_template_directory_uri() . '/assets/images/banner_cover_default.jpg';?>" srcset="<?php echo esc_attr( $product_banner_cover_srcset ); ?>" sizes="<?php echo esc_attr( $product_banner_cover_sizes ); ?>" alt="<?php echo esc_attr( $product_banner_cover_alt ); ?>" decoding="async" loading="lazy" />
    <div class="relative container mx-auto flex flex-col sm:gap-y-1 lg:gap-y-3">
        <h2 class="text-black font-bold text-2xl md:text-3xl lg:text-4xl 2xl:text-5xl"><?php echo $name_cat; ?></h2>
        <?php
            if(tr_term_field('title_sub_products_vietan', 'category', $term_id)) {
                echo '<p class="text-gray-900 font-normal text-base md:text-lg xl:text-xl">'.tr_term_field('title_sub_products_vietan', 'category', $term_id).'</p>';
            }
        ?>	
    </div>
</section>
<div class="container mx-auto py-2.5 md:py-4">
    <nav id="breadcrumbs" class="flex" aria-label="breadcrumb">
        <ol class="inline-flex items-center space-x-1 md:space-x-3">
            <li class="inline-flex items-center">
                <a class="no-underline whitespace-nowrap inline-flex items-center font-medium text-xs text-gray-400 hover:text-primary-600 dark:text-gray-600 dark:hover:text-white" title="<?php echo esc_html__( 'Home', 'BoldLab' ); ?>" href="<?php echo get_bloginfo('url');?>">
                    <svg class="w-5 h-4 md:w-5 md:h-5 mr-1 md:mr-2" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M10.7069 2.293C10.5194 2.10553 10.2651 2.00021 9.99992 2.00021C9.73475 2.00021 9.48045 2.10553 9.29292 2.293L2.29292 9.293C2.11076 9.4816 2.00997 9.7342 2.01224 9.9964C2.01452 10.2586 2.11969 10.5094 2.3051 10.6948C2.49051 10.8802 2.74132 10.9854 3.00352 10.9877C3.26571 10.99 3.51832 10.8892 3.70692 10.707L3.99992 10.414V17C3.99992 17.2652 4.10528 17.5196 4.29281 17.7071C4.48035 17.8946 4.7347 18 4.99992 18H6.99992C7.26514 18 7.51949 17.8946 7.70703 17.7071C7.89456 17.5196 7.99992 17.2652 7.99992 17V15C7.99992 14.7348 8.10528 14.4804 8.29281 14.2929C8.48035 14.1054 8.7347 14 8.99992 14H10.9999C11.2651 14 11.5195 14.1054 11.707 14.2929C11.8946 14.4804 11.9999 14.7348 11.9999 15V17C11.9999 17.2652 12.1053 17.5196 12.2928 17.7071C12.4803 17.8946 12.7347 18 12.9999 18H14.9999C15.2651 18 15.5195 17.8946 15.707 17.7071C15.8946 17.5196 15.9999 17.2652 15.9999 17V10.414L16.2929 10.707C16.4815 10.8892 16.7341 10.99 16.9963 10.9877C17.2585 10.9854 17.5093 10.8802 17.6947 10.6948C17.8801 10.5094 17.9853 10.2586 17.9876 9.9964C17.9899 9.7342 17.8891 9.4816 17.7069 9.293L10.7069 2.293Z"></path></svg>
        <?php echo esc_html__( 'Home', 'BoldLab' ); ?></a>
            </li>
            <li class="whitespace-nowrap inline-flex items-center text-gray-400  item-custom-post-type-product">
                <svg class="w-5 h-4 md:w-5 md:h-5"mr-1 md: fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M7.29303 14.707C7.10556 14.5195 7.00024 14.2652 7.00024 14C7.00024 13.7348 7.10556 13.4805 7.29303 13.293L10.586 10L7.29303 6.70701C7.11087 6.51841 7.01008 6.26581 7.01236 6.00361C7.01463 5.74141 7.1198 5.4906 7.30521 5.30519C7.49062 5.11978 7.74143 5.01461 8.00363 5.01234C8.26583 5.01006 8.51843 5.11085 8.70703 5.29301L12.707 9.29301C12.8945 9.48054 12.9998 9.73485 12.9998 10C12.9998 10.2652 12.8945 10.5195 12.707 10.707L8.70703 14.707C8.5195 14.8945 8.26519 14.9998 8.00003 14.9998C7.73487 14.9998 7.48056 14.8945 7.29303 14.707Z"></path></svg>
                <a class="no-underline ml-1 xl:ml-2 font-medium text-xs hover:text-primary-600 bread-cat bread-custom-post-type-product" href="<?php echo get_post_type_archive_link('product');?>" title="<?php echo esc_html__( 'Products', 'BoldLab' ); ?>"><?php echo esc_html__( 'Products', 'BoldLab' ); ?></a></li>
            <li class="inline-flex items-center text-gray-400 item-49">
                <svg class="w-5 h-4 md:w-5 md:h-5"mr-1 md: fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M7.29303 14.707C7.10556 14.5195 7.00024 14.2652 7.00024 14C7.00024 13.7348 7.10556 13.4805 7.29303 13.293L10.586 10L7.29303 6.70701C7.11087 6.51841 7.01008 6.26581 7.01236 6.00361C7.01463 5.74141 7.1198 5.4906 7.30521 5.30519C7.49062 5.11978 7.74143 5.01461 8.00363 5.01234C8.26583 5.01006 8.51843 5.11085 8.70703 5.29301L12.707 9.29301C12.8945 9.48054 12.9998 9.73485 12.9998 10C12.9998 10.2652 12.8945 10.5195 12.707 10.707L8.70703 14.707C8.5195 14.8945 8.26519 14.9998 8.00003 14.9998C7.73487 14.9998 7.48056 14.8945 7.29303 14.707Z"></path></svg>
                <span class="text-gray-400 ml-1 xl:ml-2 font-medium text-xs"><?php	echo $name_cat ?> - <?php echo tr_term_field('title_sub_products_vietan', 'category', $term_id);?></span>
            </li>
        </ol>
    </nav>
</div>

<section class="pt-4 pb-16 md:pt-8 md:pb-20 lg:pb-28 lg:pt-16 container mx-auto" data-aos="fade-up">
    <div class="text-center">            
        <svg class="w-[100px] h-[80px] sm:w-[117px] sm:h-[84px] fill-primary-800 mx-auto mb-8 md:mb-12" viewBox="0 0 117 84" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M114.168 2.83931C115.724 4.39579 116.598 6.50654 116.598 8.70741C116.598 10.9083 115.724 13.019 114.168 14.5755L47.7681 80.9755C46.2116 82.5315 44.1008 83.4056 41.9 83.4056C39.6991 83.4056 37.5883 82.5315 36.0319 80.9755L2.83186 47.7755C1.31995 46.2101 0.483356 44.1135 0.502267 41.9373C0.521178 39.761 1.39408 37.6793 2.93297 36.1404C4.47185 34.6015 6.5536 33.7286 8.72983 33.7097C10.9061 33.6908 13.0027 34.5274 14.5681 36.0393L41.9 63.3712L102.432 2.83931C103.988 1.2833 106.099 0.40918 108.3 0.40918C110.501 0.40918 112.612 1.2833 114.168 2.83931Z"/>
        </svg>
        <?php if(tr_post_field('title')) {
            echo '<h2 class="font-inter font-bold !leading-tight text-[32px] md:text-4xl xl:text-5xl 2xl:text-6xl mb-6 md:mb-10 text-primary-800">'.tr_post_field('title').'</h2>';
        } ?>
        <!-- <div class="text-black font-semibold text-lg sm:text-xl lg:text-2xl mb-6"><?php if(tr_post_field('your_order_code')) {echo '<span class="block md:inline-block">'.tr_post_field('your_order_code')."</span>";} echo " ".$name_cat;if($order_code) {echo "-".$order_code;}?></div> -->
        <div class="text-black font-semibold text-lg sm:text-xl lg:text-2xl mb-6"><?php if(tr_post_field('your_order_code')) {echo '<span class="block md:inline-block">'.tr_post_field('your_order_code')."</span>";} if($order_code) {echo ' ' .$order_code;}?></div>
        
        <?php if(tr_post_field('confirmation_email')) {
            echo '<div class="max-w-3xl mx-auto text-black font-normal text-lg sm:text-xl lg:text-2xl">'.tr_post_field('confirmation_email').'</div>';
        } 
        ?>     
    </div>
    
</section>

<?php
get_footer();
