<?php
/**
 * The slidebar for our theme
 *
 * This is the template that displays the `slidebar` element and everything up
 * until the `#content` element.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BoldLab
 */

?>
<div id="slidebar" class="fixed overflow-y-auto overflow-x-hidden top-0 max-md:min-h-[90px] max-md:bg-white md:bottom-0 left-0 w-full md:w-80 md:border-r-2 md:border-r-black z-10">
    <?php get_template_part( 'template-parts/layout/header', 'content' ); ?>
</div>