<?php
/**
 * The header for our theme
 *
 * This is the template that displays the `head` element and everything up
 * until the `#content` element.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BoldLab
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php wp_body_open(); ?>
<div class="flex">
	<?php get_template_part( 'slidebar' ); ?>
	<div id="page" class="md:ml-80 py-0 px-7 sm:px-8 md:px-16 lg:px-[120px] w-full md:w-[calc(100%-320px)]">
		
		<div id="content" class="wrapper mr-auto ml-0 <?php echo is_search() ? "sm:max-w-[750px]": "sm:max-w-[32em]"?> max-md:pt-32 py-[30px]">
