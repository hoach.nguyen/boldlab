<?php

/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package BoldLab
 */

get_header();
?>
<div class="post">
	<?php echo '<h2>' . get_the_title() . '</h2>'; ?>
	<?php
	the_content(
		sprintf(
			wp_kses(
				/* translators: %s: Name of current post. Only visible to screen readers. */
				__('Continue reading<span class="sr-only"> "%s"</span>', 'boldlab'),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		)
	);

	echo '<div class="my-[1.1em] date-post uppercase leading-7 font-SourceSansPro border-t border-t-black w-full text-[0.8em] font-semibold text-black">
			' . get_the_date('F j, Y') . '
		</div>';
	if (function_exists('share_social')) {
		share_social(get_the_ID());
	}
	?>
</div>

<?php echo previous_post_link(); ?>
<?php echo next_post_link(); ?>
<div class="grid grid-cols-3 gap-5 mt-8 mb-12">
	<?php
	$current_post_id = get_the_ID();
	$next_post_id = get_next_post() ? get_next_post()->ID : null;
    $previous_post_id = get_previous_post() ? get_previous_post()->ID : null;

	$exclude_post_ids = array($current_post_id);
	if ($next_post_id) {
		$exclude_post_ids[] = $next_post_id;
	}
	if ($previous_post_id) {
		$exclude_post_ids[] = $previous_post_id;
	}


	$previous_post_link = get_previous_post_link('<div class="bg-black [&>*]:block text-center [&>*]:text-white [&>*]:no-underline hover:bg-[#1e2b38] transition-all [&>*]:hover:no-underline border border-[#2c3e50] shadow-lg rounded-lg [&>*]:py-2.5 [&>*]:px-5 tracking-[0.02em] uppercase font-bold text-sm">%link</div>', 'Back');
	if ($previous_post_link) {
		echo $previous_post_link;
	}
	// Display the previous post link if available
	?>

	<?php

	$random_post_args = array(
		'orderby'        => 'rand',
		'posts_per_page' => 1,
		'post__not_in' => array($current_post_id),
	);

	$random_post_query = new WP_Query($random_post_args);

	if ($random_post_query->have_posts()) {
		while ($random_post_query->have_posts()) {
			$random_post_query->the_post();
			echo '<a class="bg-black block text-center text-white no-underline hover:bg-[#1e2b38] transition-all hover:no-underline border border-[#2c3e50] shadow-lg rounded-lg py-2.5 px-5 tracking-[0.02em] uppercase font-bold text-sm" href="' . get_permalink() . '">Random</a>';
		}
	}
	wp_reset_postdata();
	// Display the random post link
	// $random_post_link = get_permalink(get_random_post()->get_the_ID());
	// if ($random_post_link) {
	//     echo '<a class="bg-black block text-center text-white no-underline hover:bg-[#1e2b38] transition-all hover:no-underline border border-[#2c3e50] shadow-lg rounded-lg py-2.5 px-5 tracking-[0.02em] uppercase font-bold text-sm" href="' . esc_url($random_post_link) . '">Random</a>';
	// }

	?>

	<?php
	$next_post_link = get_next_post_link('<div class="bg-black [&>*]:block text-center [&>*]:text-white [&>*]:no-underline hover:bg-[#1e2b38] transition-all [&>*]:hover:no-underline border border-[#2c3e50] shadow-lg rounded-lg [&>*]:py-2.5 [&>*]:px-5 tracking-[0.02em] uppercase font-bold text-sm">%link</div>', 'Next');
	if ($next_post_link) {
		echo $next_post_link;
	}
	?>
</div>

<?php
get_footer();
