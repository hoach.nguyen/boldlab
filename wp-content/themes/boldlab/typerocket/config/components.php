<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Component Registry
    |--------------------------------------------------------------------------
    */
    'registry' => [
        'content' => \App\Components\ContentComponent::class,
        'recentpopularposts' => \App\Components\RecentPopularPostsComponent::class,
        'alltimemostpopularposts' => \App\Components\AllTimeMostPopularPostsComponent::class,
        'top100' => \App\Components\Top100Component::class,
        'storyoftheweek' => \App\Components\StoryoftheweekComponent::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Builder
    |--------------------------------------------------------------------------
    |
    | List of components you want included for the builder group.
    |
    */
    'builder' => [
        'content',
        'recentpopularposts',
        'alltimemostpopularposts',
        'top100',
        'storyoftheweek',
    ]
];