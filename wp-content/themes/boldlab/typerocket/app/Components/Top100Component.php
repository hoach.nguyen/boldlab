<?php
namespace App\Components;

use TypeRocket\Template\Component;

class Top100Component extends Component
{
    protected $title = 'Top 100 Component';

    /**
     * Admin Fields
     */
    public function fields()
    {
        $form = $this->form();

        echo $form->text('Headline');
        echo $form->textarea('Description');
        echo $form->search('top100post')->setLabel('Select post Top 100')->setPostTypeOptions('post')->multiple();
    }

    /**
     * Render
     *
     * @var array $data component fields
     * @var array $info name, item_id, model, first_item, last_item, component_id, hash
     */
    public function render(array $data, array $info)
    {
        ?>
        <div class="builder-content">
            <?php
                if($data['headline']) {
                    echo '<h2>'.esc_html($data['headline']).'</h2>';
                }
                if($data['description']) {
                    echo '<div><small>'.esc_html($data['description']).'</small></div>';
                }
                $top100 = $data['top100post'];
                if (is_array($top100) && sizeof($top100) > 0) {
                    echo '<div class="mt-5">';
                    foreach ($top100 as $postIem) {
                        $content = get_the_content(null, false, $postIem);
                        $stripped_content = wp_strip_all_tags($content);
                        $trimmed_content = mb_substr($stripped_content, 0, 200); // Change 200 to the desired character limit

                        echo '<h3><a class="no-underline text-black text-lg" href="' . get_the_permalink($postIem) . '" title="'.get_the_title($postIem).'">'.get_the_title($postIem).'</a></h3>
                        <p class="!mt-1">'.$trimmed_content.'</p>';
                    }
                    echo '</div>';
                }
            ?>
            
        </div>
        <?php
    }
}