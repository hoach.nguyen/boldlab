<?php
namespace App\Components;

use TypeRocket\Template\Component;

class AllTimeMostPopularPostsComponent extends Component
{
    protected $title = 'AllTime Most Popular Posts Component';

    /**
     * Admin Fields
     */
    public function fields()
    {
        $form = $this->form();

        echo $form->text('Headline')->setDefault('All-Time Most Popular Posts');
        echo $form->toggle('autocode_alltime')->setLabel('All time most popular posts is being retrieved automatically ')->setText("If you don't want to get it automatically, open and select all time most popular posts");
        echo $form->section(  
            $form->search('all_time_most_popular')->setLabel('Select All Time Most Popular Posts')->setPostTypeOptions('post')->multiple()
        )->when('autocode_alltime');
    }

    /**
     * Render
     *
     * @var array $data component fields
     * @var array $info name, item_id, model, first_item, last_item, component_id, hash
     */
    public function render(array $data, array $info)
    {
        ?>
        <div class="builder-content">
            <?php
            if($data['autocode_alltime'] == 1) {
                $all_time_most_popular = $data['all_time_most_popular'];
                if (is_array($all_time_most_popular) && sizeof($all_time_most_popular) > 0) {
                    if($data['headline']) {
                        echo '<h2>'.esc_html($data['headline']).'</h2>';
                    }
                    echo '<ol class="mt-5 list-decimal ml-10 flex flex-col gap-4">';
                    foreach ($all_time_most_popular as $postIem) {
                        echo '<li><a class="no-underline text-black hover:text-[#ffb900] hover:no-underline" href="' . get_the_permalink($postIem) . '" title="'.get_the_title($postIem).'">'.get_the_title($postIem).'</a></li>';
                    }
                    echo '</ol>';
                }
            } else {
                if($data['headline']) {
                    echo '<h2>'.esc_html($data['headline']).'</h2>';
                }
                if(function_exists('all_time_most_popular_posts')){all_time_most_popular_posts();}
            }
            ?>
        </div>
        <?php
    }
}