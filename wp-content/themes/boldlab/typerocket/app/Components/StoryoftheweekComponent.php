<?php
namespace App\Components;

use TypeRocket\Template\Component;

class StoryoftheweekComponent extends Component
{
    protected $title = 'Story of the week Component';

    /**
     * Admin Fields
     */
    public function fields()
    {
        $form = $this->form();
        echo $form->text('Headline')->setDefault('STORY OF THE WEEK');
        echo $form->text('Description')->setDefault('The most viewed post of the week. Picked by you.');
        echo $form->toggle('autocode_week')->setLabel('Story of the week is being retrieved automatically ')->setText("If you don't want to get it automatically, open and select the article");
        echo $form->section(  
            $form->search('storyoftheweek')->setLabel('Select Story of the week')->setPostTypeOptions('post'),
        )->when('autocode_week');
        
    }

    /**
     * Render
     *
     * @var array $data component fields
     * @var array $info name, item_id, model, first_item, last_item, component_id, hash
     */
    public function render(array $data, array $info)
    {
        ?>
        <div class="builder-content mb-5">
            <?php
                if($data['headline']) {
                    echo '<h3 class="!text-[#ffb900] uppercase !text-[13px]">'.esc_html($data['headline']).'</h3>';
                }
                if($data['description']) {
                    echo '<div class="text-[#ffb900] text-[15px]">'.esc_html($data['description']).'</div>';
                }
                if($data['autocode_week'] == 1) {
                    $storyoftheweek = $data['storyoftheweek'];
                    if($storyoftheweek) {
                        echo '<div class="mt-8"><h2>'.get_the_title($storyoftheweek).'</h2>';
                        $post_content = get_post_field('post_content', $storyoftheweek);
                        echo wp_kses(
                            wpautop(sprintf(
                                /* translators: %s: Name of current post. Only visible to screen readers. */
                                __( '%s Continue reading<span class="sr-only"> "%s"</span>', 'boldlab' ),
                                $post_content,
                                get_the_title($storyoftheweek)
                            )),
                            array(
                                'span' => array(
                                    'class' => array(),
                                ),
                                'a' => array(
                                    'href' => array(),
                                ),
                                'p' => array(), // Allow paragraph tags
                            )
                        );  
    
                           
                        echo '<div class="my-[1.1em] date-post uppercase leading-7 font-SourceSansPro border-t border-t-black w-full text-[0.8em] font-semibold text-black">
                            '.get_the_date('F j, Y',$storyoftheweek).'
                        </div>';
                        if(function_exists('share_social')){share_social($storyoftheweek);}
                        
                        echo '</div>';
                    }
                } else {
                    if(function_exists('get_most_viewed_post_of_week')){get_most_viewed_post_of_week();}
                }
            ?>
        </div>
        <?php
    }
}