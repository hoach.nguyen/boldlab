<?php
namespace App\Components;

use TypeRocket\Template\Component;

class RecentPopularPostsComponent extends Component
{
    protected $title = 'Recent Popular Posts Component';

    /**
     * Admin Fields
     */
    public function fields()
    {
        $form = $this->form();

        echo $form->text('Headline')->setDefault('Recent Popular Posts');
        echo $form->toggle('autocode_popular_posts')->setLabel('Recent Popular Posts is being retrieved automatically ')->setText("If you don't want to get it automatically, open and select recent popular posts");
        echo $form->section(  
            $form->search('recent_popular_posts')->setLabel('Select Recent Popular Posts')->setPostTypeOptions('post')->multiple(),
        )->when('autocode_popular_posts');
    }

    /**
     * Render
     *
     * @var array $data component fields
     * @var array $info name, item_id, model, first_item, last_item, component_id, hash
     */
    public function render(array $data, array $info)
    {
        ?>
        <div class="builder-content">
            <?php
                if($data['headline']) {
                    echo '<h2>'.esc_html($data['headline']).'</h2>';
                }
                if($data['autocode_popular_posts'] == 1) {
                    $recent_popular_posts = $data['recent_popular_posts'];
                    if (is_array($recent_popular_posts) && sizeof($recent_popular_posts) > 0) {
                        echo '<ol class="mt-5 mb-10 list-decimal ml-10 flex flex-col gap-4">';
                        foreach ($recent_popular_posts as $postIem) {
                            echo '<li><a class="no-underline text-black font-bold hover:text-[#ffb900] hover:no-underline" href="' . get_the_permalink($postIem) . '" title="'.get_the_title($postIem).'">'.get_the_title($postIem).'</a></li>';
                        }
                        echo '</ol>';
                    }
                } else {
                    if(function_exists('recent_popular_posts')){recent_popular_posts();}
                }
            ?>
        </div>
        <?php
    }
}