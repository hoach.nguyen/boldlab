<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no `home.php` file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BoldLab
 */

get_header();
?>

<?php
	echo '<input type="hidden" name="post_types" value="archive">';
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	$posts_per_page = get_option('posts_per_page');
	$query_args = array(
		'post_type' => 'post',
		'post_status' => 'publish',
		'orderby' => 'date',
		'order' => 'DESC',
		'paged'  => $paged, 
		'posts_per_page'=> $posts_per_page,
	);
	$query_post = new WP_Query( $query_args);
	if( $query_post->have_posts() ) {
		echo '<div class="flex flex-col gap-[30px]" data-scroll-post="true" id="wrapper-post">';
			while( $query_post->have_posts() ) { $query_post->the_post();
				echo '<div class="post">
					<h2><a href="'.get_permalink(get_the_ID()).'" title="'.get_the_title(get_the_ID()).'">'.get_the_title().'</a></h2>';
					the_content(
						sprintf(
							wp_kses(
								/* translators: %s: Name of current post. Only visible to screen readers. */
								__( 'Continue reading<span class="sr-only"> "%s"</span>', 'boldlab' ),
								array(
									'span' => array(
										'class' => array(),
									),
								)
							),
							get_the_title()
						)
					);
					echo '<div class="my-[1.1em] date-post uppercase leading-7 font-SourceSansPro border-t border-t-black w-full text-[0.8em] font-semibold text-black">
						'.get_the_date('F j, Y').'
					</div>';
					if(function_exists('share_social')){share_social(get_the_ID());}
				echo '</div>';
			}
		echo '</div>';
		
		wp_reset_postdata();
	}
?>
	

<?php
get_footer();
