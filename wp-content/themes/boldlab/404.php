<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package BoldLab
 */

get_header();$unique_id_c = esc_attr( uniqid( 'search-form-' ) );
?>

	<section id="primary" class="post">
		<main id="main">
			<h2 class="page-title"><?php esc_html_e( "We can't find what you're looking for.", 'boldlab' ); ?></h2>
			<div <?php boldlab_content_class( 'page-content' ); ?>>
				<p><?php esc_html_e( 'Oof... Sorry for the technical headache. Search below to find what you were looking for or email us at 404@boldlab.com to tell us what broke.', 'boldlab' ); ?></p>
				<form role="search" method="get" class="search-form font-SourceSansPro" action="<?php echo esc_url( home_url( '/' ) ); ?>">
				<div class="relative border border-black rounded-[5px] flex overflow-hidden">
					<input id="<?php echo $unique_id_c; ?>" class="search-field border-none bg-transparent h-10 md:h-9 w-full max-w-[calc(100%-62px)] p-[5px] outline-none text-sm" type="search" value="<?php echo get_search_query(); ?>" name="s" required />
					<button type="submit" class="search-submit text-white bg-black border-0 h-10 md:h-9 w-1/4 min-w-[62px] flex justify-center items-center flex-shrink-0">
						<svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" viewBox="-2972 1546 21.43 21.43"><g transform="translate(-3222 879)"><path style="fill:#fff;" d="M21.43,19.755l-6.362-6.529a8.1,8.1,0,0,0,1.674-4.855A8.4,8.4,0,0,0,8.371,0,8.29,8.29,0,0,0,0,8.371a8.4,8.4,0,0,0,8.371,8.371,8.1,8.1,0,0,0,4.855-1.674l6.362,6.362ZM1.674,8.371a6.7,6.7,0,1,1,6.7,6.7A6.716,6.716,0,0,1,1.674,8.371Z" transform="translate(250 667)"/></g></svg>	
					</button>
				</div>
			</form>
			</div><!-- .page-content -->
		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
