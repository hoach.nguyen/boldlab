<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BoldLab
 */

get_header();
	$post_types = get_post_type();
	// $term_id = get_queried_object_id(); 
	$queried_object = get_queried_object();
	$taxonomy = $queried_object->taxonomy;
	$term_id = $queried_object->term_id;
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	$posts_per_page = get_option('posts_per_page');
	echo '<input type="hidden" name="post_types" value="'.$post_types.'">';
	echo '<input type="hidden" name="taxonomy" value="'.$taxonomy.'">';
	echo '<input type="hidden" name="term_id" value="'.$term_id.'">';
	?>
	<?php
		$args = array(
			'post_type'      => $post_types, // Điều này có thể thay đổi tùy vào loại bài viết của bạn
			'post_status' => 'publish',
			'orderby' => 'date',
			'order' => 'DESC',
			'paged'  => $paged, 
			'posts_per_page' => $posts_per_page,
			'tax_query'      => array(
				array(
					'taxonomy' => $taxonomy,
					'field'    => 'term_id',
					'terms'    => $term_id,
				),
			),
		);
		$query_post = new WP_Query( $args);
		if( $query_post->have_posts() ) {
			echo '<div class="flex flex-col gap-10 mt-10" data-scroll-post="true" id="wrapper-post">';
				while( $query_post->have_posts() ) { $query_post->the_post();
					echo '<div class="post">
						<h2><a href="'.get_permalink(get_the_ID()).'" title="'.get_the_title(get_the_ID()).'">'.get_the_title().'</a></h2>';
						the_content(
							sprintf(
								wp_kses(
									/* translators: %s: Name of current post. Only visible to screen readers. */
									__( 'Continue reading<span class="sr-only"> "%s"</span>', 'boldlab' ),
									array(
										'span' => array(
											'class' => array(),
										),
									)
								),
								get_the_title()
							)
						);
						echo '<div class="my-[1.1em] date-post uppercase leading-7 font-SourceSansPro border-t border-t-black w-full text-[0.8em] font-semibold text-black">
							'.get_the_date('F j, Y').'
						</div>';
						if(function_exists('share_social')){share_social(get_the_ID());}
					echo '</div>';
				}
			echo '</div>';
			
			wp_reset_postdata();
		}
	?>
	
<?php
get_footer();
