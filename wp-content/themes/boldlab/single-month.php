<?php
/**
 * The template for displaying archive pages by month.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BoldLab
 */

get_header();

// Lấy thông tin tháng và năm từ URL
$current_year = get_query_var('year');
$current_month = get_query_var('monthnum');

// Tạo tiêu đề cho trang
$page_title = 'Archives for ' . date('F Y', mktime(0, 0, 0, $current_month, 1, $current_year));
?>
archive-month.php
<main id="primary" class="site-main">
    <header class="page-header">
        <h1 class="page-title"><?php echo $page_title; ?></h1>
    </header><!-- .page-header -->

    <div class="archive-content">
        <?php
        // Include template for displaying posts by month
        get_template_part('template-parts/content', 'month');
        ?>
    </div><!-- .archive-content -->

</main><!-- #main -->

<?php
get_footer();
?>
