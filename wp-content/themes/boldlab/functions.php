<?php
/**
 * BoldLab functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package BoldLab
 */

if ( ! defined( 'BOLDLAB_VERSION' ) ) {
	/*
	 * Set the theme’s version number.
	 *
	 * This is used primarily for cache busting. If you use `npm run bundle`
	 * to create your production build, the value below will be replaced in the
	 * generated zip file with a timestamp, converted to base 36.
	 */
	define( 'BOLDLAB_VERSION', 'sfzk9k' );
}

if ( ! defined( 'BOLDLAB_TYPOGRAPHY_CLASSES' ) ) {
	/*
	 * Set Tailwind Typography classes for the front end, block editor and
	 * classic editor using the constant below.
	 *
	 * For the front end, these classes are added by the `boldlab_content_class`
	 * function. You will see that function used everywhere an `entry-content`
	 * or `page-content` class has been added to a wrapper element.
	 *
	 * For the block editor, these classes are converted to a JavaScript array
	 * and then used by the `./javascript/block-editor.js` file, which adds
	 * them to the appropriate elements in the block editor (and adds them
	 * again when they’re removed.)
	 *
	 * For the classic editor (and anything using TinyMCE, like Advanced Custom
	 * Fields), these classes are added to TinyMCE’s body class when it
	 * initializes.
	 */
	define(
		'BOLDLAB_TYPOGRAPHY_CLASSES',
		'not-prose prose-neutral max-w-none prose-a:text-primary'
	);
}

if ( ! function_exists( 'boldlab_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function boldlab_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on BoldLab, use a find and replace
		 * to change 'boldlab' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'boldlab', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'menu-1' => __( 'Primary', 'boldlab' ),
				'menu-2' => __( 'Footer Menu', 'boldlab' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Add support for editor styles.
		add_theme_support( 'editor-styles' );

		// Enqueue editor styles.
		add_editor_style( 'style-editor.css' );
		add_editor_style( 'style-editor-extra.css' );

		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );

		// Remove support for block templates.
		remove_theme_support( 'block-templates' );
	}
endif;
add_action( 'after_setup_theme', 'boldlab_setup' );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function boldlab_widgets_init() {
	register_sidebar(
		array(
			'name'          => __( 'Footer', 'boldlab' ),
			'id'            => 'sidebar-1',
			'description'   => __( 'Add widgets here to appear in your footer.', 'boldlab' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'boldlab_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function boldlab_scripts() {
	wp_enqueue_style( 'boldlab-style', get_stylesheet_uri(), array(), BOLDLAB_VERSION );
	

	wp_enqueue_script( 'jquery-core', includes_url('/js/jquery/jquery.min.js') ,array(), '3.7.1', true);
	wp_enqueue_script( 'boldlab-script', get_template_directory_uri() . '/js/script.min.js', array(), BOLDLAB_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'boldlab_scripts' );



/**
 * Enqueue the block editor script.
 */
function boldlab_enqueue_block_editor_script() {
	wp_enqueue_script(
		'boldlab-editor',
		get_template_directory_uri() . '/js/block-editor.min.js',
		array(
			'wp-blocks',
			'wp-edit-post',
		),
		BOLDLAB_VERSION,
		true
	);
}
add_action( 'enqueue_block_editor_assets', 'boldlab_enqueue_block_editor_script' );

/**
 * Enqueue the script necessary to support Tailwind Typography in the block
 * editor, using an inline script to create a JavaScript array containing the
 * Tailwind Typography classes from BOLDLAB_TYPOGRAPHY_CLASSES.
 */
function boldlab_enqueue_typography_script() {
	if ( is_admin() ) {
		wp_enqueue_script(
			'boldlab-typography',
			get_template_directory_uri() . '/js/tailwind-typography-classes.min.js',
			array(
				'wp-blocks',
				'wp-edit-post',
			),
			BOLDLAB_VERSION,
			true
		);
		wp_add_inline_script( 'boldlab-typography', "tailwindTypographyClasses = '" . esc_attr( BOLDLAB_TYPOGRAPHY_CLASSES ) . "'.split(' ');", 'before' );
	}
}
add_action( 'enqueue_block_assets', 'boldlab_enqueue_typography_script' );

/**
 * Add the Tailwind Typography classes to TinyMCE.
 *
 * @param array $settings TinyMCE settings.
 * @return array
 */
function boldlab_tinymce_add_class( $settings ) {
	$settings['body_class'] = BOLDLAB_TYPOGRAPHY_CLASSES;
	return $settings;
}
add_filter( 'tiny_mce_before_init', 'boldlab_tinymce_add_class' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

// function revcon_change_post_label() {
// 	global $menu;
// 	global $submenu;
// 	$menu[5][0] = 'News and updates';
// 	$submenu['edit.php'][5][0] = 'News and updates';
// 	$submenu['edit.php'][10][0] = 'Add News and updates';
// 	$submenu['edit.php'][16][0] = 'Tags News and updates';
// }
// add_action( 'admin_menu', 'revcon_change_post_label' );

// Disable the emoji's
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );	
remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );	
remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email');
add_filter( 'emoji_svg_url', '__return_false');

wp_deregister_script('wp-embed');
remove_action ('wp_head', 'rsd_link');
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action('wp_head', 'wp_generator');

function tp_admin_logo() {
    echo '<br/><img style="max-height:80px;max-width:100%;margin-top:10px" alt="'.get_bloginfo( 'name' ).'" src="'. get_template_directory_uri() .'/images/logo.svg"/>';
}
add_action( 'admin_notices', 'tp_admin_logo' );

function tp_admin_footer_credits( $text ) {
  $text='<p>Welcome to the website <a href="'.get_bloginfo( 'url' ).'"  title="'.get_bloginfo( 'name' ).'"><strong>'.get_bloginfo( 'name' ).'</strong></a></p>';
   return $text;
 }
add_filter( 'admin_footer_text', 'tp_admin_footer_credits' );
function custom_loginlogo() {
echo '<style type="text/css">
h1 a {height:100px !important;background-image: url("'. get_template_directory_uri() .'/images/logo.svg") !important; background-size: contain  !important;width: auto !important;}
</style>';
}
add_action('login_head', 'custom_loginlogo');
function custom_login_link() {
    return get_site_url(); // Replace with your desired link
}
add_filter('login_headerurl', 'custom_login_link');


// Allow SVG
add_filter( 'wp_check_filetype_and_ext', function($data, $file, $filename, $mimes) {

	global $wp_version;
	if ( $wp_version !== '4.7.1' ) {
	   return $data;
	}
  
	$filetype = wp_check_filetype( $filename, $mimes );
  
	return [
		'ext'             => $filetype['ext'],
		'type'            => $filetype['type'],
		'proper_filename' => $data['proper_filename']
	];
  
  }, 10, 4 );
  
  function cc_mime_types( $mimes ){
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
  }
  add_filter( 'upload_mimes', 'cc_mime_types' );
  
  function fix_svg() {
	echo '<style type="text/css">
		  img[width="1"] {
			   min-width: 150px !important;
			   height: auto !important;
		  }
		  .tr-taxonomy-edit-container .tr-fieldset-group {display:table-caption}
		  </style>';
  }
  add_action( 'admin_head', 'fix_svg' );
  
  //end allow upload svg

//remove svg body 
remove_action ('wp_body_open', 'wp_global_styles_render_svg_filters');
// Activate Typerocket plugin
require get_template_directory() . '/typerocket/init.php';
require get_template_directory() . '/inc/typerocket.php';
//Theme-option
add_filter('typerocket_theme_options_controller', function($controller) {
	return function($themeOptions) {
		 $form = tr_form()->useRest()->setGroup( $themeOptions->getName() );
		return tr_view( __DIR__ . '/theme-options.php', ['form' => $form]);
	};
});

// Remove <p> and <br/> from Contact Form 7
add_filter('wpcf7_autop_or_not', '__return_false');
//add autocomplete
add_filter( 'wpcf7_form_autocomplete', function ( $autocomplete ) {
    $autocomplete = 'off';
    return $autocomplete;
}, 10, 1 );

// function create_subscribers_table() {
//     global $wpdb;
//     $table_name = $wpdb->prefix . 'subscribers';
//     $charset_collate = $wpdb->get_charset_collate();

//     $sql = "CREATE TABLE IF NOT EXISTS $table_name (
//         id INT AUTO_INCREMENT PRIMARY KEY,
//         email VARCHAR(255) NOT NULL,
//         UNIQUE KEY unique_email (email)
//     ) $charset_collate;";

//     require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
//     dbDelta($sql);
// }

// add_action('init', 'create_subscribers_table');

function share_social($postID) { ?>
	<div class="flex items-center justify-end gap-3">
		<a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink($postID)).'&amp;title='.get_the_title($postID); ?>"  title="<?php echo get_the_title($postID);?>" class="text-white bg-black w-6 h-6 rounded flex items-center justify-center" target="_blank">
			<svg class="w-4 h-4" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M80 299.3V512H196V299.3h86.5l18-97.8H196V166.9c0-51.7 20.3-71.5 72.7-71.5c16.3 0 29.4 .4 37 1.2V7.9C291.4 4 256.4 0 236.2 0C129.3 0 80 50.5 80 159.4v42.1H14v97.8H80z"/></svg>
		</a>
		<a href="http://twitter.com/intent/tweet?text=<?php echo get_the_title($postID); ?>+<?php echo get_permalink($postID); ?>" title="<?php echo get_the_title($postID); ?>" class="text-white bg-black w-6 h-6 rounded flex items-center justify-center" target="_blank">
			<svg class="w-4 h-4" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M459.4 151.7c.3 4.5 .3 9.1 .3 13.6 0 138.7-105.6 298.6-298.6 298.6-59.5 0-114.7-17.2-161.1-47.1 8.4 1 16.6 1.3 25.3 1.3 49.1 0 94.2-16.6 130.3-44.8-46.1-1-84.8-31.2-98.1-72.8 6.5 1 13 1.6 19.8 1.6 9.4 0 18.8-1.3 27.6-3.6-48.1-9.7-84.1-52-84.1-103v-1.3c14 7.8 30.2 12.7 47.4 13.3-28.3-18.8-46.8-51-46.8-87.4 0-19.5 5.2-37.4 14.3-53 51.7 63.7 129.3 105.3 216.4 109.8-1.6-7.8-2.6-15.9-2.6-24 0-57.8 46.8-104.9 104.9-104.9 30.2 0 57.5 12.7 76.7 33.1 23.7-4.5 46.5-13.3 66.6-25.3-7.8 24.4-24.4 44.8-46.1 57.8 21.1-2.3 41.6-8.1 60.4-16.2-14.3 20.8-32.2 39.3-52.6 54.3z"/></svg>
		</a>
		<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo urlencode(get_permalink($postID)); ?>&amp;title=<?php echo get_the_title($postID); ?>&amp;source=<?php echo site_url();?>" class="text-white bg-black w-6 h-6 rounded flex items-center justify-center" target="_blank">
			<svg class="w-4 h-4" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path d="M100.3 448H7.4V148.9h92.9zM53.8 108.1C24.1 108.1 0 83.5 0 53.8a53.8 53.8 0 0 1 107.6 0c0 29.7-24.1 54.3-53.8 54.3zM447.9 448h-92.7V302.4c0-34.7-.7-79.2-48.3-79.2-48.3 0-55.7 37.7-55.7 76.7V448h-92.8V148.9h89.1v40.8h1.3c12.4-23.5 42.7-48.3 87.9-48.3 94 0 111.3 61.9 111.3 142.3V448z"/></svg>
		</a>
		<a class="text-white bg-black w-6 h-6 rounded flex items-center justify-center js-copy-url relative" href="<?php echo get_permalink($postID); ?>">
			<svg class="w-4 h-4" fill="currentColor" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path d="M579.8 267.7c56.5-56.5 56.5-148 0-204.5c-50-50-128.8-56.5-186.3-15.4l-1.6 1.1c-14.4 10.3-17.7 30.3-7.4 44.6s30.3 17.7 44.6 7.4l1.6-1.1c32.1-22.9 76-19.3 103.8 8.6c31.5 31.5 31.5 82.5 0 114L422.3 334.8c-31.5 31.5-82.5 31.5-114 0c-27.9-27.9-31.5-71.8-8.6-103.8l1.1-1.6c10.3-14.4 6.9-34.4-7.4-44.6s-34.4-6.9-44.6 7.4l-1.1 1.6C206.5 251.2 213 330 263 380c56.5 56.5 148 56.5 204.5 0L579.8 267.7zM60.2 244.3c-56.5 56.5-56.5 148 0 204.5c50 50 128.8 56.5 186.3 15.4l1.6-1.1c14.4-10.3 17.7-30.3 7.4-44.6s-30.3-17.7-44.6-7.4l-1.6 1.1c-32.1 22.9-76 19.3-103.8-8.6C74 372 74 321 105.5 289.5L217.7 177.2c31.5-31.5 82.5-31.5 114 0c27.9 27.9 31.5 71.8 8.6 103.9l-1.1 1.6c-10.3 14.4-6.9 34.4 7.4 44.6s34.4 6.9 44.6-7.4l1.1-1.6C433.5 260.8 427 182 377 132c-56.5-56.5-148-56.5-204.5 0L60.2 244.3z"/></svg>
		</a>
	</div>
<?php }

function load_more_archive_posts() {
	$posts_per_page = get_option('posts_per_page');
    $paged = $_POST['page'];
    $year = $_POST['year'];
    $month = $_POST['month'];
	$offset = ($paged - 1) * $posts_per_page;
	$args = array(
		'post_type' => 'post',
		'posts_per_page' => $posts_per_page,
		'offset' => $offset,
		'year' => $year,
		'monthnum' => $month,
	);
	$query = new WP_Query($args);

    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            // Output your post content here as needed
            echo '<div class="post">
				<h2><a href="'.get_permalink(get_the_ID()).'" title="'.get_the_title(get_the_ID()).'">'.get_the_title().'</a></h2>';
				the_content(
					sprintf(
						wp_kses(
							/* translators: %s: Name of current post. Only visible to screen readers. */
							__( 'Continue reading<span class="sr-only"> "%s"</span>', 'boldlab' ),
							array(
								'span' => array(
									'class' => array(),
								),
							)
						),
						get_the_title()
					)
				);
				echo '<div class="my-[1.1em] date-post uppercase leading-7 font-SourceSansPro border-t border-t-black w-full text-[0.8em] font-semibold text-black">
					'.get_the_date('F j, Y').'
				</div>';
				if(function_exists('share_social')){share_social(get_the_ID());}
			echo '</div>';
        }
    } else {
        echo 'no-more-posts'; // Signal that there are no more posts
    }

    wp_die();
}

add_action('wp_ajax_load_more_archive_posts', 'load_more_archive_posts');
add_action('wp_ajax_nopriv_load_more_archive_posts', 'load_more_archive_posts');

function load_more_news_posts() {
	$posts_per_page = get_option('posts_per_page');
    $paged = $_POST['page'];
    $post_types = $_POST['post_types'];
    $taxonomy = $_POST['taxonomy'];
    $term_id = $_POST['term_id'];
	$offset = ($paged - 1) * $posts_per_page;

	$args = array(
		'post_type' => $post_types,
		'post_status' => 'publish',
		'orderby' => 'date',
		'order' => 'DESC',
		// 'paged'  => $paged, 
		'posts_per_page'=> $posts_per_page,
		'offset' => $offset,
	);
	if (!empty($taxonomy) && !empty($term_id)) {
        $args['tax_query'] = array(
            array(
                'taxonomy' => $taxonomy,
                'field'    => 'id',
                'terms'    => $term_id,
            ),
        );
    }
	$query = new WP_Query($args);

    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            // Output your post content here as needed
			echo '<div class="post">
				<div>
					<span class="font-black block !text-[#ffb900] no-underline text-sm uppercase">'.(get_term( $term_id )->name ?	get_term( $term_id )->name : '').'</span>
					<h2><a href="'.get_permalink(get_the_ID()).'" title="'.get_the_title(get_the_ID()).'">'.get_the_title().'</a></h2>
				</div>';
				the_content(
					sprintf(
						wp_kses(
							/* translators: %s: Name of current post. Only visible to screen readers. */
							__( 'Continue reading<span class="sr-only"> "%s"</span>', 'boldlab' ),
							array(
								'span' => array(
									'class' => array(),
								),
							)
						),
						get_the_title()
					)
				);
				echo '<div class="my-[1.1em] date-post uppercase leading-7 font-SourceSansPro border-t border-t-black w-full text-[0.8em] font-semibold text-black">
					'.get_the_date('F j, Y').'
				</div>';
				if(function_exists('share_social')){share_social(get_the_ID());}
			echo '</div>';
        }
    } else {
        echo 'no-more-posts'; // Signal that there are no more posts
    }

    wp_die();
}
add_action('wp_ajax_load_more_news_posts', 'load_more_news_posts');
add_action('wp_ajax_nopriv_load_more_news_posts', 'load_more_news_posts');

function load_more_cat_posts() {
	$posts_per_page = get_option('posts_per_page');
    $paged = $_POST['page'];
    $post_types = $_POST['post_types'];
    $taxonomy = $_POST['taxonomy'];
    $term_id = $_POST['term_id'];
	$offset = ($paged - 1) * $posts_per_page;

	$args = array(
		'post_type' => $post_types,
		'post_status' => 'publish',
		'orderby' => 'date',
		'order' => 'DESC',
		// 'paged'  => $paged, 
		'posts_per_page'=> $posts_per_page,
		'offset' => $offset,
	);
	if (!empty($taxonomy) && !empty($term_id)) {
        $args['tax_query'] = array(
            array(
                'taxonomy' => $taxonomy,
                'field'    => 'id',
                'terms'    => $term_id,
            ),
        );
    }
	$query = new WP_Query($args);

    if ($query->have_posts()) {
        while ($query->have_posts()) {
            $query->the_post();
            // Output your post content here as needed
			echo '<div class="post">
				<div>
					<h2><a href="'.get_permalink(get_the_ID()).'" title="'.get_the_title(get_the_ID()).'">'.get_the_title().'</a></h2>
				</div>';
				the_content(
					sprintf(
						wp_kses(
							/* translators: %s: Name of current post. Only visible to screen readers. */
							__( 'Continue reading<span class="sr-only"> "%s"</span>', 'boldlab' ),
							array(
								'span' => array(
									'class' => array(),
								),
							)
						),
						get_the_title()
					)
				);
				echo '<div class="my-[1.1em] date-post uppercase leading-7 font-SourceSansPro border-t border-t-black w-full text-[0.8em] font-semibold text-black">
					'.get_the_date('F j, Y').'
				</div>';
				if(function_exists('share_social')){share_social(get_the_ID());}
			echo '</div>';
        }
    } else {
        echo 'no-more-posts'; // Signal that there are no more posts
    }

    wp_die();
}
add_action('wp_ajax_load_more_cat_posts', 'load_more_cat_posts');
add_action('wp_ajax_nopriv_load_more_cat_posts', 'load_more_cat_posts');


function highlight_search_results($text) {
    if (is_search()) {
        $search_query = get_search_query();
        $text = preg_replace('/(' . $search_query . ')/i', '<mark class="bg-[#ffb900] text-black">$1</mark>', $text);
    }
    return $text;
}
add_filter('the_content', 'highlight_search_results');
add_filter('the_excerpt', 'highlight_search_results');

function custom_trim_excerpt($text, $length = 20) {
    $words = explode(' ', $text, $length + 1);

    if (count($words) > $length) {
        array_pop($words);
        $text = implode(' ', $words) . '...';
    }

    return $text;
}

// Filter the excerpt
add_filter('get_the_excerpt', 'custom_trim_excerpt');



// add_filter('rewrite_rules_array', function( $rules ) {
// 	$taxonomy = get_terms( array(
//         'taxonomy' => 'category',
//         'hide_empty' => false
//     ) );
// 	foreach($taxonomy as $term) {
//         // $rule = $term->slug . '/?$';
// 		$rule = $term->slug . '/?$';
//         $query = 'index.php?category_name=' . $term->slug;
//         if ( !array_key_exists($rule, $rules) ) {
// 			$new_rules[$rule] = $query;
//         }
// 		$en_term = get_term_meta($term->term_id, '_trp_translated_slug_en_US', true);
// 		$rule = $en_term . '/?$';
// 		$query = 'index.php?category_name=' . $term->slug;
// 		if ( !array_key_exists($rule, $rules) ) {
// 			$new_rules[$rule] = $query;
//         }
//     }
// 	$taxonomy = get_terms( array(
//         'taxonomy' => 'archive_categories', //thay bang custom taxonomy
//         'hide_empty' => false
//     ) );
// 	foreach($taxonomy as $term) {
//         // $rule = $term->slug . '/?$';
// 		$rule = $term->slug . '/?$';
//         $query = 'index.php?archive_categories=' . $term->slug;
//         if ( !array_key_exists($rule, $rules) ) {
// 			$new_rules[$rule] = $query;
//         }
// 		$en_term = get_term_meta($term->term_id, '_trp_translated_slug_en_US', true);
// 		$rule = $en_term . '/?$';
// 		$query = 'index.php?archive_categories=' . $term->slug;
// 		if ( !array_key_exists($rule, $rules) ) {
// 			$new_rules[$rule] = $query;
//         }
//     }
// 	return $new_rules + $rules;
//  } );
//  add_filter( 'term_link', function( $permalink, $term ) {
//         // Check if the term has a parent
// 	if ($term->parent != 0) {
// 		// Get the parent term
// 		$parent_term = get_term($term->parent, $term->taxonomy);
// 		// Append parent term's year to the child term's slug
// 		$permalink = home_url($parent_term->slug . '/' . $term->slug . '/');
// 	} else {
// 		if( $term->taxonomy == 'category' ) $permalink = home_url( $term->slug . '/' );
// 		if( $term->taxonomy == 'archive_categories' ) $permalink = home_url( $term->slug . '/' );

// 	}
// 	return $permalink;
// }, 10, 2 );



// add_filter('post_link', 'term_link', 10, 2);
// add_filter('post_type_link', 'term_link', 10, 2);

// add_action('wpcf7_before_send_mail', 'subscribe_to_list');
// function subscribe_to_list($contact_form) {
//     $form_id = $contact_form->id();
	
//     // Kiểm tra nếu đây là form đăng ký
//     if ($form_id == '8') {
//         $submission = WPCF7_Submission::get_instance();
//         if ($submission) {
//             $posted_data = $submission->get_posted_data();

//             // Lấy giá trị email từ dữ liệu gửi đi
//             $email = sanitize_email($posted_data['your-email']);
			
// 			// Kiểm tra xem email đã tồn tại trong post type "subscribe" chưa
// 			$existing_post = get_page_by_title($email, OBJECT, 'subscribe');

// 			if ($existing_post) {
// 				$contact_form->set_response('Email already exists. Please use a different email.');
                
//                 // Invalid form submission to prevent email sending
//                 $result = WPCF7_Validation::get_instance($contact_form);
//                 $tag = new WPCF7_FormTag($contact_form, array('type' => 'email', 'name' => 'your-email'));
//                 $result->invalidate($tag, "Email already exists. Please use a different email.");
// 			} else {
// 				// Tạo bài viết kiểu tùy chỉnh với tiêu đề là địa chỉ email
// 				$post_data = array(
// 					'post_title' => $email,
// 					'post_type' => 'subscribe',
// 					'post_status' => 'publish',
// 				);
// 				// Thêm bài viết mới
// 				$post_id = wp_insert_post($post_data);
// 				//echo "<script> window.location = 'https://batdongsanttp.vn'</script>";
//                 // exit;
// 			}
//         }
//     }
// }

add_filter('wpcf7_validate_email*', function( $result, $tag ) {
	$email = sanitize_email($_POST['your-email']);
	$existing_post = get_page_by_path(sanitize_title($email), OBJECT, 'subscribe');
	if ($existing_post) {
		$result->invalidate( $tag, 'Email already exists. Please use a different email.' );
	} else {
		// Tạo bài viết kiểu tùy chỉnh với tiêu đề là địa chỉ email
		$post_data = array(
			'post_title' => $email,
			'post_type' => 'subscribe',
			'post_status' => 'publish',
		);
		// Thêm bài viết mới
		$post_id = wp_insert_post($post_data);
	}
	return $result;
}, 10, 2);



// Hàm gửi email đến danh sách người đăng ký
function send_email_to_subscribers($new_status, $old_status, $post) {
    // Kiểm tra xem bài viết được xuất bản
    if ($new_status === 'publish' && $old_status !== 'publish') {
        $post_type = get_post_type($post);
		$blog_name = get_bloginfo('name');
        // Kiểm tra xem bài viết thuộc custom post type "subscribe" hay không
        if ($post_type === 'post') {
            // Lấy danh sách email từ custom post type "subscribe"
            $subscribers = get_posts(array(
                'post_type' => 'subscribe',
                'posts_per_page' => -1,
            ));

            // Lặp qua danh sách và gửi email cho mỗi người đăng ký
            foreach ($subscribers as $subscriber) {
                $email = get_the_title($subscriber->ID);
                $subject = $blog_name . ': ' . get_the_title($post->ID);
				$post_content = apply_filters('the_content', get_post_field('post_content', $post->ID));
                $message = '<h3><a href="' . get_permalink($post->ID) . '" title="' . esc_attr(get_the_title($post->ID)) . '">' . get_the_title($post->ID) . '</a></h3>';
				$message .= '<div>' . $post_content . '</div>';
				$message .= '<p style="margin-top:8px;color:gray">…<br><font size="2"></font><small>You\'re getting this note because you subscribed to '.$blog_name.'. Don\'t want to get this email anymore? Click the link below to unsubscribe. <font size="2"></font></small></p>';
                // Gửi email với định dạng HTML
				$headers = array('Content-Type: text/html; charset=UTF-8');
				wp_mail($email, $subject, $message, $headers);
            }
        }
    }
}
add_action('transition_post_status', 'send_email_to_subscribers', 10, 3);


add_action( 'restrict_manage_posts', 'add_export_button' );
function add_export_button() {
    $screen = get_current_screen();

    //if (isset($screen->parent_file) && ('edit.php' == $screen->parent_file)) { add for post
    if($screen->post_type != 'subscribe' ) {
    	return;
    }
        ?>
        <input type="submit" name="export_all_email" id="export_all_email" class="button button-primary" value="Export All Email">
        <script type="text/javascript">
            jQuery(function($) {
                $('#export_all_email').insertAfter('#post-query-submit');
            });
        </script>
        <?php
}

//Export post type to excel file
add_action( 'init', 'func_export_all_email' );
function func_export_all_email() {
    if(isset($_GET['export_all_email'])) {
        $arg = array(
            'post_type' => 'subscribe',
            'post_status' => 'publish',
            'posts_per_page' => -1,
        );
        global $post;
				$arr_post = get_posts($arg);
        if ($arr_post) {
            header('Content-type: text/csv');
            header('Content-Disposition: attachment; filename="list-email.csv"');
            header('Pragma: no-cache');
            header('Expires: 0');
            $file = fopen('php://output', 'w');
            fputcsv($file, array('List Email Subscribes'));
            foreach ($arr_post as $post) {
            	// $email_text = get_post_meta($post->ID, 'email', TRUE );
            	$email_text = get_the_title($post->ID);
                setup_postdata($post);
                //fputcsv($file, array(get_the_title(), get_the_permalink()));
                fputcsv($file, array($email_text));
            }
            exit();
        }
    }
}


function track_post_views() {
    if (is_single()) {
        global $post;
        $post_id = $post->ID;
        $views = get_post_meta($post_id, 'post_views_count', true);
        $views = $views ? $views + 1 : 1;

        update_post_meta($post_id, 'post_views_count', $views);
    }
}
add_action('wp_head', 'track_post_views');

//STORY OF THE WEEK
function get_most_viewed_post_of_week($number_of_posts=1) {
	$week = date('W'); 
	$year = date('Y');
    $args = array(
        'post_type'      => 'post',
        'posts_per_page' => $number_of_posts,
        'meta_key'       => 'post_views_count',
        'orderby'        => 'meta_value_num',
        'order'          => 'DESC',
		'weeknummer' => $week,
		'year' => $year,
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) {
        $query->the_post();
        // Output the post content or other details
        echo '<div class="mt-8"><h2>'.get_the_title().'</h2>';
		// $post_content = get_post_field('post_content',get_the_ID());
		the_content(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers. */
					__( 'Continue reading<span class="sr-only"> "%s"</span>', 'boldlab' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			)
		);

			
		echo '<div class="my-[1.1em] date-post uppercase leading-7 font-SourceSansPro border-t border-t-black w-full text-[0.8em] font-semibold text-black">
			'.get_the_date('F j, Y',get_the_ID()).'
		</div>';
		if(function_exists('share_social')){share_social(get_the_ID());}
		
		echo '</div>';
    }

    wp_reset_postdata();
}
//Recent Popular post
function recent_popular_posts($number_of_posts=10) {
    $args = array(
        'post_type'      => 'post',
        'posts_per_page' => $number_of_posts,
        'meta_key'       => 'post_views_count',
        'orderby'        => 'meta_value_num',
        'order'          => 'DESC',
    );

    $query = new WP_Query($args);
    if ($query->have_posts()) {
		echo '<ol class="mt-5 mb-10 list-decimal ml-10 flex flex-col gap-4">';
			while ($query->have_posts()) {
				$query->the_post();
				// Output the post content or other details
				echo '<li><a class="no-underline text-black font-bold hover:text-[#ffb900] hover:no-underline" href="' . get_the_permalink() . '" title="'.get_the_title().'">'.get_the_title().'</a></li>';
			}
		echo '</ol>';
    }

    wp_reset_postdata();
}
//Top 100 All-Time Most Popular Posts:
function all_time_most_popular_posts($number_of_posts=35) {
	$counter = 0;
    $args = array(
        'post_type'      => 'post',
        'posts_per_page' => $number_of_posts, // Set the number of posts to retrieve
        'meta_key'       => 'post_views_count',
        'orderby'        => 'meta_value_num',
        'order'          => 'DESC',
		// 'post__not_in'   => $recent_post_ids,
    );

    $query = new WP_Query($args);

    if ($query->have_posts()) {
		echo '<ol class="my-5 list-decimal ml-10 flex flex-col gap-4">';
			while ($query->have_posts()) {
				$query->the_post();
				// Skip the first 9 posts
				if ($counter++ < 10) {
					continue;
				}
				echo '<li><a class="no-underline text-black hover:text-[#ffb900] hover:no-underline" href="' . get_the_permalink() . '" title="'.get_the_title().'">'.get_the_title().'</a></li>';
			}
		echo '</ol>';
    }

    wp_reset_postdata();
}

