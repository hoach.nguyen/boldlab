<?php
add_action('typerocket_loaded', function() {

	$editorSettings = [
		'teeny' => false,
		'tinymce' => [
			'toolbar3' => '',
		],
	];

	

	$news = tr_post_type('News');
	$news->addSupport('thumbnail');
	$news->setIcon('dashicons-screenoptions');
	$news->setTitlePlaceholder('Enter News title here');
	$news->setSlug('news');
	$news->setTitleForm(function() use ($editorSettings) {
		$form = tr_form();
		
	});
	$news->enableGutenberg();

	// $product_categories = tr_taxonomy('Archive Categories');
	// $product_categories->setId('archive_categories');
	// $product_categories->setSlug('archive_categories');
	// $product_categories->setHierarchical();
	// $product_categories->addPostType('archive');
	// $product_categories->addPostType('post');
	// $product_categories->showQuickEdit(true);
	// $product_categories->setRest('true');//Hiện lên khi có  enableGutenberg()
	
	tr_post_type('attachment')->setTitleForm(function() {
	   $form = tr_form();
	   echo $form->text('Target URL');
	});

	$subscribe = tr_post_type('Subscribe');
	$subscribe->addSupport('thumbnail');
	$subscribe->setIcon('dashicons-email-alt2');
	$subscribe->setTitlePlaceholder('Enter subscribe title here');
	$subscribe->setSlug('subscribe');
	

	
});



?>