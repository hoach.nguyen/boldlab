<h1>Theme Options</h1>  
<?php  
/** @var \TypeRocket\Elements\Form $form */  
echo $form->useRest()->open();
 

// Header  
$header = $form->fieldset('Company', 'Details about your company.', [   
  $form->text('company_name')->setLabel('Company Name'),
  $form->image('Company Logo'),  
  $form->input('Company Email')->setTypeEmail(),
  $form->text('Company Phone'),
  // $form->text('company_hotline')->setLabel('Company Hotline'),
  // $form->image('Icon Hotline'),  
  // $form->image('Icon Email'),  
  // $form->text('header_button_target')->setLabel('Button Target'), 
]);

// $menu_options = [
//     'Custom Link' => 'custom_link',
//     'Page' => 'page',
//     'Category' => 'category',
//     'Product Category' => 'product_cat',
//     'Product Post' => 'product',
// ];

// $sub_type = [
//   'Dropdown' => 'dropdown',
//   'Mega Menu' => 'mega',
// ];

// // Menu
// $menu = $form->fieldset('Menu', '', [   
//   $form->repeater('Menu Item')->setFields([
//         $form->select('Menu Type')->setOptions($menu_options),
//         $form->text('Label')->when('menu_type', 'custom_link'),
//         $form->text('Custom Link')->when('menu_type', 'custom_link'),
//         $form->search('Search Page')->setPostTypeOptions('page')->when('menu_type', 'page'),
//         $form->search('Search Category')->setTaxonomyOptions('category')->when('menu_type', 'category'),
//         $form->search('Search Service Category')->setTaxonomyOptions('service_cat')->when('menu_type', 'service_cat'),
//         $form->search('Search Service')->setPostTypeOptions('page')->when('menu_type', 'service'),
//         $form->checkbox('Sub Menu')->setText('Yes'),
//         $form->select('Sub Type')->setOptions($sub_type)->when('sub_menu')
//   ])
// ]);

$socials = $form->fieldset('Social Account', '', [   
  $form->text('txt_follow_us')->setLabel('Title Follow Us')->setDefault('Follow Us'),
  $form->repeater('social_items')->setLabel('Social Item')->setFields([
    $form->row(
      $form->image('Icon Social'), 
      $form->text('Link social'),
      $form->checkbox('URL open in new window'),
    ),
  ])
]);
  // $form->text('social_facebook_link')->setLabel('Facebook Link'),
  // $form->image('social_facebook_icon')->setLabel('Icon'),  

  // $form->text('social_instagram_link')->setLabel('Instagram Link'),
  // $form->image('social_instagram_icon')->setLabel('Icon'),  
  // $form->text('social_tiktok_link')->setLabel('Tiktok Link'),
  // $form->image('social_tiktok_icon')->setLabel('Icon'),  
  // $form->text('social_youtube_link')->setLabel('Youtube Link'),
  // $form->image('social_youtube_icon')->setLabel('Icon'),  
  // $form->text('social_twitter_link')->setLabel('Twitter Link'),
  // $form->image('social_twitter_icon')->setLabel('Icon'), 
  // $form->text('social_linkedin_link')->setLabel('Linkedin Link'),
  // $form->image('social_linkedin_icon')->setLabel('Icon'),  

// $cta = $form->fieldset('Call To Action', 'A Call To Action (CTA) is an element on your site that asks people to do something specific.', [   
//   $form->repeater('cta_items')->setLabel('CTA Item')->setFields([
//         $form->image('Icon')->setAdminImageSize('full'),
//         $form->text('Title'),
//         $form->textarea('Description'),
//         $form->text('Button Label'),
//         $form->text('Button Target'),
//   ])
// ]);

// $fbcoment = $form->fieldset('Facebook comment setting', '', [   
//   $form->password('Facebook App Id'),  
//   $form->password('Facebook Access Token'),  
//   $form->input('Number post')->setTypeNumber(),
// ]);
// $image_default = $form->fieldset('Image photo when the post has no Feature image', '', [   
//   $form->image('Image default'),
//   $form->image('Image 404'),
// ]);

// $floating = $form->fieldset('Floating Button', 'A floating button is a type of button that appears on the top layer of a webpage so it is always visible even if you scroll down the page.', [   
//   $form->repeater('floating_button')->setLabel('Button Item')->setFields([
//     $form->image('Icon')->setAdminImageSize('full'),  
//     $form->text('Target'),  
//     $form->checkbox('Background White')->setText('Yes')
//   ])
// ]); 

// $footer_contact = [
//     $form->image('Footer Logo'),  
//     $form->row(
//       $form->text('Footer Phone'),
//       $form->text('Footer Email')
//     ),
//     $form->text('Footer Address'),
// ];
// $footer_column1 = [
//   $form->image('Footer Logo'),  
//   $form->text('Footer Title'),
//   // $form->textarea('Footer Description'),
//   $form->repeater('contact_company')->setLabel('Info contact')->setFields([
//     $form->image('Icon'),  
//     $form->text('Text'),
//     $form->text('link_target')->setLabel('Input link target'),
//   ]),
//   $form->image('Background footer'),  

// ];
// $footer_navLink = [
//   $form->repeater('nav_items')->setLabel('Nav Link Items')->setFields([
//     $form->text('title_heading')->setLabel('Title heading'),
//     $form->repeater('items_link')->setLabel('Link Items')->setFields([
//       $form->row(
//         $form->text('title_link')->setLabel('Title link'),
//         $form->text('link')->setLabel('Link'),
//       ),
//     ])
//   ])
// ];
$slideBar = [
  $form->repeater('left_slidebar_left')->setLabel('List block')->setFields([
    $form->text('Title row'),
    $form->repeater('list_block')->setLabel('List items')->setFields([
      $form->row(
        $form->text('Title'),
        $form->text('Link href'),
        $form->checkbox('URL open in new window'),
      ),      
    ]),
  ]),
];
// $copyright = [
//   $form->text('Footer Copyright'),
// ];

// $tabs = tr_tabs();
// $tabs->tab('Section Get Started', 'pencil', $get_started);
// $tabs->tab('Info company', 'pencil', $footer_column1);
// $tabs->tab('Nav Links', 'pencil', $footer_navLink);
// $tabs->tab('Copyright', 'droplet', $copyright);

// // Footer  
// $footer = $form->fieldset('Footer', '', $tabs); 

//Error
// $PageNotFound = [
//   $form->text('pagenotfound_title')->setLabel('Title')->setDefault('Ohhh! Page&nbsp;Not&nbsp;Found'),
//   $form->textarea('pagenotfound_description')->setLabel('Description'),
//   $form->row(
//     $form->text('pagenotfound_btn_title')->setLabel('Button title'),
//     $form->text('pagenotfound_btn_target')->setLabel('Button target'),
//     $form->checkbox('pagenotfound_btn_openwindow')->setLabel('URL open in new window'),
//   ),
//   $form->image('pagenotfound_img')->setLabel('Images'),  
//   $form->row(
//     $form->image('pagenotfound_bg')->setLabel('Background'),  
//     $form->image('pagenotfound_bg_m')->setLabel('Background mobile'),  
//   ),
// ];


// $tabs_error = tr_tabs();
// $tabs_error->tab('Page Not Found', '', $PageNotFound);
// $Error = $form->fieldset('Error', '', $tabs_error); 

// All Product  
// $all_products = $form->fieldset('Page All Product', '', [   
//   $form->text('Product Title Banner')->setLabel('Title Banner'),
//   $form->textarea('Product Description Banner')->setLabel('Description Banner'),
//   $form->image('Product Images Banner'),  
//   $form->text('Product Title'),
//   $form->wpEditor('Product Description'),
//   $form->text('product_why_iMisff_special')->setLabel('Why iMisff special?'),
//   $form->repeater('product_why_special_items')->setLabel('Product why special items')->setFields([
//     $form->image('Images'),  
//     $form->text('Title'),
//   ]),
//   $form->fieldset('Meta for Share Link On Social', '', [   
//     $form->text('Product Title Meta'),  
//     $form->textarea('Product Description Meta'),  
//     $form->image('Product Thumbnail Meta'),  
//   ]),
// ]);




// Save  
$save = $form->submit( 'Save Changes' );  
// Layout  
$tabs = tr_tabs()->setFooter( $save )->layoutLeft();  
$tabs->tab('ABOUT', 'dashicons-building', $header)->setDescription('Company information');
$tabs->tab('Slide Bar', 'dashicons-info', $slideBar)->setDescription('Slide bar Left');
$tabs->tab('Social Account', 'dashicons-share', $socials)->setDescription('Great Social Account');
//$tabs->tab('Facebook Comment', 'dashicons-facebook-alt', $fbcoment)->setDescription('Facebook comment');
//$tabs->tab('Call To Action', 'dashicons-megaphone', $cta)->setDescription('Your CTA');
//$tabs->tab('Floating Button', 'dashicons-phone', $floating)->setDescription('Customize the Floating Button');
// $tabs->tab('Footer', 'dashicons-table-row-before', $footer)->setDescription('Footer information');
// $tabs->tab('All Product', 'dashicons-screenoptions', $all_products)->setDescription('Page All Product');
// $tabs->tab('Image Default', 'dashicons-format-image', $image_default)->setDescription('Image default');
// $tabs->tab('Error', 'dashicons-dismiss', $Error)->setDescription('Error information');
// $tabs->tab('No open roles', 'dashicons-groups', $NoOpenRoles)->setDescription('No open roles');
// $tabs->tab('Q&A', 'dashicons-twitch', $qa)->setDescription('Q&A');
// $tabs->tab('Search', 'dashicons-search', $search)->setDescription('Search');
//$tabs->tab('Blogs', 'dashicons-welcome-write-blog', $blogs_tab)->setDescription('Blogs information');

$tabs->render();  
  
echo $form->close();  
?>
