<?php
/**
 * Template part for displaying pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BoldLab
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
		if ( ! is_front_page() ) {
			the_title( '<h2 class="mb-5 font-SourceSansPro max-sm:text-[1.8em] text-[2.2em] font-black leading-[1.1em] -tracking-[0.02em] text-black">', '</h2>' );
		} else {
			the_title( '<h2 class="mb-5 font-SourceSansPro max-sm:text-[1.8em] text-[2.2em] font-black leading-[1.1em] -tracking-[0.02em] text-black">', '</h2>' );
		}
	?>

	<?php //boldlab_post_thumbnail(); ?>

	<div <?php boldlab_content_class( 'entry-content' ); ?>>
		<?php
		the_content();

		// wp_link_pages(
		// 	array(
		// 		'before' => '<div>' . __( 'Pages:', 'boldlab' ),
		// 		'after'  => '</div>',
		// 	)
		// );
		?>
	</div><!-- .entry-content -->

</article><!-- #post-<?php the_ID(); ?> -->
