<?php
/**
 * Template part for displaying single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BoldLab
 */
$categories = get_the_category();
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div>
	<?php if ($categories) {
		$category_link = get_category_link($categories[0]->term_id);
		echo '<a href="'.$category_link.'" title="'.$categories[0]->name.'" class="font-black block !text-[#ffb900] no-underline text-sm uppercase">'.$categories[0]->name.'</a>';
	}
	?>
		<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
	</div>

	<?php //boldlab_post_thumbnail(); ?>

	<div <?php boldlab_content_class( 'entry-content' ); ?>>
		<?php
		the_content(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers. */
					__( 'Continue reading<span class="sr-only"> "%s"</span>', 'boldlab' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			)
		);

		// wp_link_pages(
		// 	array(
		// 		'before' => '<div>' . __( 'Pages:', 'boldlab' ),
		// 		'after'  => '</div>',
		// 	)
		// );
		echo '<div class="my-[1.1em] date-post uppercase leading-7 font-SourceSansPro border-t border-t-black w-full text-[0.8em] font-semibold text-black">
			'.get_the_date('F j, Y').'
		</div>';
		if(function_exists('share_social')){share_social(get_the_ID());}
		?>
	</div><!-- .entry-content -->
	
</article><!-- #post-${ID} -->
