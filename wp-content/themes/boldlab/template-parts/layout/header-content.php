<?php
/**
 * Template part for displaying the header content
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package BoldLab
 */
$unique_id_c = esc_attr( uniqid( 'search-form-' ) );

?>
<div id="header-border" class="min-h-[7px] md:min-h-2 w-full absolute top-[75px] border-t border-t-black max-md:border-b-[7px] max-md:border-b-[#ffb900] md:top-0 left-0 z-[2] bg-[#ff8c00] md:bg-[#ffb900] md:border-t-8 md:border-t-[#ff8c00] box-content"></div>
<div id="header-top" class="md:mt-[30px] md:px-[30px] max-md:flex max-md:items-center max-md:justify-between max-md:px-[15px] max-md:pt-[8px] max-md:pb-[7px] max-md:mb-[15px]">
	<a class="no-underline hover:no-underline flex items-center outline-none" href="<?php echo get_site_url()?>" title="<?php echo get_bloginfo('name');?>">
	<?php
		$company_logo = tr_option_field('tr_theme_options.company_logo') ? wp_get_attachment_image_url(tr_option_field('tr_theme_options.company_logo'), 'full') : get_template_directory_uri() . '/images/logo.png';
		if($company_logo) {
			echo '<img width="100" height="100" class="object-contain w-full max-sm:max-h-[52px] max-md:max-h-[60px] max-h-[100px] h-auto img-logo" src="'.$company_logo.'" alt="'.get_bloginfo('name').'">';
		} 
		$company_name = tr_option_field('tr_theme_options.company_name') ? tr_option_field('tr_theme_options.company_name') : get_bloginfo('name');
		if($company_name) {
			echo '<h1 class="text-[#231f20] font-bold text-2xl md:text-3xl uppercase opacity-0 absolute invisible">'.$company_name.'</h1>';
		} 
	?>
	</a>
	<div class="btn-m md:hidden">			
		<span class="cursor-pointer" id="menu-open"><svg class="w-[30px] block" xmlns="http://www.w3.org/2000/svg" viewBox="-1150 949 28 17">
			<g id="Symbol_8_8" transform="translate(-1478 917)">
				<rect id="Rectangle_40" width="28" height="3" transform="translate(328 32)"></rect>
				<rect id="Rectangle_41" width="28" height="3" transform="translate(328 39)"></rect>
				<rect id="Rectangle_42" width="28" height="3" transform="translate(328 46)"></rect>
			</g>
			</svg>
		</span>
		<span class="cursor-pointer hidden" id="menu-close"><svg class="w-[30px] block" xmlns="http://www.w3.org/2000/svg" viewBox="-1255 1000 21.92 21.92">
			<g id="Symbol_9_6" data-name="Symbol 9 – 6" transform="translate(-3209.04 896.46)">
				<rect id="Rectangle_40" width="28" height="3" transform="translate(1956.161 103.54) rotate(45)"></rect>
				<rect id="Rectangle_41" width="28" height="3" transform="translate(1954.04 123.339) rotate(-45)"></rect>
			</g>
			</svg>
		</span>
	</div>
</div>
<section class="sidebar-content p-[30px] hidden md:block">
	<div class="flex flex-col gap-[30px] ">
		<div class="flex flex-col gap-[5px]">
			<h2 class="text-lg md:text-base font-SourceSansPro font-black leading-5 -tracking-[.02em] text-black">Search</h2>
			<form role="search" method="get" class="search-form font-SourceSansPro" action="<?php echo esc_url( home_url( '/' ) ); ?>">
				<div class="relative border border-black rounded-[5px] flex overflow-hidden">
					<input id="<?php echo $unique_id_c; ?>" class="search-field border-none bg-transparent h-10 md:h-9 w-full max-w-[calc(100%-62px)] p-[5px] outline-none text-sm" type="search" value="<?php echo get_search_query(); ?>" name="s" required />
					<button type="submit" class="search-submit text-white bg-black border-0 h-10 md:h-9 w-1/4 min-w-[62px] flex justify-center items-center flex-shrink-0">
						<svg class="w-6 h-6" xmlns="http://www.w3.org/2000/svg" viewBox="-2972 1546 21.43 21.43"><g transform="translate(-3222 879)"><path style="fill:#fff;" d="M21.43,19.755l-6.362-6.529a8.1,8.1,0,0,0,1.674-4.855A8.4,8.4,0,0,0,8.371,0,8.29,8.29,0,0,0,0,8.371a8.4,8.4,0,0,0,8.371,8.371,8.1,8.1,0,0,0,4.855-1.674l6.362,6.362ZM1.674,8.371a6.7,6.7,0,1,1,6.7,6.7A6.716,6.716,0,0,1,1.674,8.371Z" transform="translate(250 667)"/></g></svg>	
					</button>
				</div>
			</form>
		</div>
		<?php
			$subscribe = '[contact-form-7 id="6288411" title="Subscribe"]';
			if (has_shortcode($subscribe, 'contact-form-7')) {
				echo do_shortcode($subscribe);
			}  
		?>
		<?php
			$left_slidebar_left = tr_option_field('tr_theme_options.left_slidebar_left');
			if(is_array($left_slidebar_left) && sizeof($left_slidebar_left) > 0){
				foreach($left_slidebar_left as $leftbar_item) {
					echo '<div class="flex flex-col gap-[5px]">';
					if($leftbar_item['title_row']) {echo '<h2 class="text-lg md:text-base font-SourceSansPro font-black leading-5 -tracking-[.02em] text-black">'.$leftbar_item['title_row'].'</h2>';}
					
					$list_block = $leftbar_item['list_block'];
					if (is_array($list_block) && sizeof($list_block) > 0) {
						echo '<ul class="list-none">';
						foreach($list_block as $blockItem) {
							
							if($blockItem['title']) {							
								echo '<li><a class="no-underline text-black text-[15px] hover:underline hover:text-[#003eff]" href="'.($blockItem['link_href'] ? $blockItem['link_href'] : '#').'" title="'.$blockItem['title'].'" '.($blockItem['url_open_in_new_window'] ? 'target="_blank"': '').'>'.$blockItem['title'].'</a></li>';
							}
						}
						echo '</ul>';
					}
					echo '</div>';

				}
			}
		?>
	</div>
</section>